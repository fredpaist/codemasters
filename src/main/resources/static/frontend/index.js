import * as ReactDOM from "react-dom";
import {applyMiddleware, createStore} from "redux";
import { Provider } from 'react-redux';
import rootReducer from "./reducers";
import React from "react";
import 'babel-polyfill'
import thunkMiddleware from 'redux-thunk';
import App from "./App";
import { composeWithDevTools } from 'redux-devtools-extension';
import {validateLogin} from "./components/Login/LoginActions";

const store = createStore(
    rootReducer,
    composeWithDevTools(
        applyMiddleware(
            thunkMiddleware,
        )
    ),
);

store.dispatch(validateLogin());

const router = (
    <Provider store={store}>
        <App />
    </Provider>
);

ReactDOM.render(router, document.getElementById('react'));