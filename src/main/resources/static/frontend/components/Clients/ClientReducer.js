import {
    CLIENT_LIST_DATA,
    CLIENT_API_ERROR,
    SELECT_CLIENT,
    CHANGE_VIEW_TYPE,
    LIST_VIEW,
    FORM_VIEW, GET_CLIENT_COUNTRIES,
    SAVE_NEW_CLIENT, GET_ACTIVE_CLIENT
} from "./ClientConstants";

const initialState = {
    error: '',
    clients: null,
    activeClientId: null,
    activeClient: null,
    currentView: LIST_VIEW,
    countries: null,
}

export default function clientReducer(state = initialState, action) {
    switch (action.type) {
        case CLIENT_LIST_DATA:
            return Object.assign({}, state, {
                ...state,
                clients: action.payload,
                error: '',
            })
        case CLIENT_API_ERROR:
            return Object.assign({}, state, {
                ...state,
                error: action.payload,
            })
        case SELECT_CLIENT:
            return Object.assign({}, state, {
                ...state,
                activeClientId: action.payload,
                currentView: FORM_VIEW
            })
        case CHANGE_VIEW_TYPE:
            return Object.assign({}, state, {
                ...state,
                currentView: action.payload,
            })
        case GET_CLIENT_COUNTRIES:
            return Object.assign({}, state, {
                ...state,
                countries: action.payload,
            })
        case SAVE_NEW_CLIENT:
            return Object.assign({}, state, {
                ...state,
                message: action.payload,
                currentView: LIST_VIEW,
                activeClientId: null,
                activeClient: null,
            })
        case GET_ACTIVE_CLIENT:
            return Object.assign({}, state, {
                ...state,
                activeClient: action.payload
            })
        default:
            return state;
    }
}
