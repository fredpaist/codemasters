import axios from "axios";
import {
    SAVE_NEW_CLIENT,
    CHANGE_VIEW_TYPE,
    CLIENT_API_ERROR,
    CLIENT_LIST_DATA,
    GET_CLIENT_COUNTRIES,
    SELECT_CLIENT, GET_ACTIVE_CLIENT
} from "./ClientConstants";
import {SubmissionError} from "redux-form";

export const getClientList = () => async dispatch => {
    const jwtHeader = {headers: {'Authorization': 'Bearer ' + localStorage.getItem("accessToken")}};

    await axios.get("api/clients", jwtHeader)
        .then(response => {
            dispatch({type: CLIENT_LIST_DATA, payload: response.data})
        })
        .catch(error => {
            dispatch({type: CLIENT_API_ERROR, payload: error})
        });
}

export const selectClient = (client) => dispatch => {
    dispatch({ type: SELECT_CLIENT, payload: client });
}

export const changeView = (view) => dispatch => {
    dispatch({type: CHANGE_VIEW_TYPE, payload: view})
}

export const addNewClient = (values) => async dispatch => {
    const jwtHeader = {headers: {'Authorization': 'Bearer ' + localStorage.getItem("accessToken")}};

    await axios.post("/api/clients", values, jwtHeader)
        .then(response => {
            dispatch({type: SAVE_NEW_CLIENT, payload: response.data});
        })
        .catch(error => {
            dispatch(throwError(error))
        })
}

export const getActiveClient = (id) => async dispatch => {
    const jwtHeader = {headers: {'Authorization': 'Bearer ' + localStorage.getItem("accessToken")}};

    await axios.get(`api/clients/${id}`, jwtHeader)
        .then(response => {
            dispatch({type: GET_ACTIVE_CLIENT, payload: response.data})
        })
        .catch(error => {
            dispatch({type: CLIENT_API_ERROR, payload: error})
        });
}

export const throwError = (error) => dispatch => {
    const {errors} = error.response.data;

    const responseErrors = {}

    for(var i in errors) {
        const { field, defaultMessage } = errors[i];
        responseErrors[field] = defaultMessage;
    }

    if(responseErrors) {
        throw new SubmissionError(responseErrors)
    }
}

export const saveClient = (id, values) => async dispatch => {
    const jwtHeader = {headers: {'Authorization': 'Bearer ' + localStorage.getItem("accessToken")}};

    await axios.put(`api/clients/${id}`, values, jwtHeader)
        .then(response => {
            dispatch({type: SAVE_NEW_CLIENT, payload: response.data});
        })
        .catch(error => {
            dispatch(throwError(error))
        })
}

export const getCountries = () => async dispatch => {
    const jwtHeader = {headers: {'Authorization': 'Bearer ' + localStorage.getItem("accessToken")}};

    await axios.get("api/countries", jwtHeader)
        .then(response => {
            dispatch({type: GET_CLIENT_COUNTRIES, payload: response.data})
        })
        .catch(error => {
            dispatch({type: CLIENT_API_ERROR, payload: error})
        });
}