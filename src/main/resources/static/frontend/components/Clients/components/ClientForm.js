import React from 'react'
import { Field, reduxForm } from 'redux-form'
import {TextInput, SelectInput} from "../../FormInputs";

const ClientForm = ({handleSubmit, activeClientId, countries}) => {
    const emailLabel = (<>Email <span className="text-muted">(Optional)</span></>);

    return (
        <form onSubmit={handleSubmit}>
            <div className="row">
                <Field className="form-control" wrapper="col-md-6 mb-3" label="First Name" name="firstName" component={TextInput} type="text" />
                <Field className="form-control" wrapper="col-md-6 mb-3" label="Last Name" name="lastName" component={TextInput} type="text" />
            </div>
            <Field className="form-control" wrapper="mb-3" label="Username" name="username" component={TextInput} type="text" placeholder="Username" />
            <Field className="form-control" wrapper="mb-3" label={emailLabel} name="email" component={TextInput} type="email" placeholder="you@example.com" />
            <Field className="form-control" wrapper="mb-3" label="Address" name="address" component={TextInput} type="text" placeholder="1234 Main St" />
            <Field className="custom-select d-block w-100" label="Country"
                   name="country" component={SelectInput} defaultValue="Choose..." options={countries} />
            <hr className="mb-4" />
            <button className="btn btn-primary btn-lg btn-block" type="submit">{activeClientId ? "Edit Client" : "Add Client"}</button>
        </form>
    )
};

const validate = values => {
    const errors = {};
    if (!values.firstName) {
        errors.firstName = 'Required'
    }

    if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Invalid email address'
    }

    if (!values.lastName) {
        errors.lastName = 'Required'
    }

    if (!values.username) {
        errors.username = 'Required'
    }

    if (!values.address) {
        errors.address = 'Required'
    }

    if (!values.country) {
        errors.country = 'Required'
    }

    return errors
};

export default reduxForm({
    // a unique name for the form
    form: 'client_data',
    validate
})(ClientForm)