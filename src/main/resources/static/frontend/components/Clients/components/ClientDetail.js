import React from "react";
import ClientForm from "./ClientForm";
import {getCountries, addNewClient, getActiveClient, saveClient} from "../ClientActions";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

class ClientDetail extends React.Component {

    async componentDidMount() {
        await this.props.getCountries();

        if (this.props.client.activeClientId) {
            this.props.getActiveClient(this.props.client.activeClientId)
        }
    }

    render() {
        const {client} = this.props;
        const countriesList = client.countries ? client.countries.countryResponseList : [];

        let submit = (values) => this.props.addNewClient(values)
        if (client.activeClientId) {
            submit = (values) => this.props.saveClient(client.activeClientId, values);
        }

        return (
            <div className="container">
                <div className="py-5 text-center">
                    <h2>{client.activeClientId ? "Edit Client" : "Add Client"}</h2>
                </div>

                <div className="row">
                    <div className="col-md-12 order-md-1">
                        {countriesList && <ClientForm activeClientId={client.activeClientId} countries={countriesList} initialValues={client.activeClient} onSubmit={submit}/>}
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        client: state.clientReducer
    }
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getCountries,
        addNewClient,
        getActiveClient,
        saveClient
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(ClientDetail)