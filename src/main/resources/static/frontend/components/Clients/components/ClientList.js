import React from "react"
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {changeView, getClientList, selectClient} from "../ClientActions";
import {FORM_VIEW} from "../ClientConstants";

class ClientList extends React.Component {

    componentDidMount() {
        this.props.getClientList();
    }

    render() {
        const {client} = this.props;
        const clientList = client.clients ? client.clients.clientResponseList : [];

        return (
            <div className="container">
                <div className="py-5 text-center">
                    <h2>Clients</h2>
                </div>
                {client.message && <div className="alert alert-success" role="alert">{client.message}</div>}
                <div className="row">
                    <a href="#" onClick={() => this.props.changeView(FORM_VIEW)} className="btn btn-primary">Add client</a>
                    <table className="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">First</th>
                            <th scope="col">Last</th>
                            <th scope="col">Username</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        {clientList &&
                            clientList.map((client, index) => {
                                return (
                                    <tr key={index}>
                                        <th scope="row">{client.id}</th>
                                        <td>{client.firstName}</td>
                                        <td>{client.lastName}</td>
                                        <td>{client.username}</td>
                                        <td>
                                            <a href="#" onClick={() => this.props.selectClient(client.id)} className="btn btn-primary">Edit client</a>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        client: state.clientReducer
    }
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getClientList,
        selectClient,
        changeView
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(ClientList)