export const CLIENT_LIST_DATA = 'CLIENT_LIST_DATA';
export const CLIENT_API_ERROR = 'CLIENT_API_ERROR';
export const SELECT_CLIENT = 'SELECT_CLIENT';
export const SAVE_NEW_CLIENT = 'SAVE_NEW_CLIENT';
export const CHANGE_VIEW_TYPE = 'CHANGE_VIEW_TYPE';
export const GET_CLIENT_COUNTRIES = 'GET_CLIENT_COUNTRIES';
export const GET_ACTIVE_CLIENT = 'GET_ACTIVE_CLIENT';

export const LIST_VIEW = 'LIST_VIEW';
export const FORM_VIEW = 'FORM_VIEW';