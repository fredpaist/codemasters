import React from "react"
import {connect} from "react-redux";
import ClientList from "./components/ClientList";
import {FORM_VIEW} from "./ClientConstants";
import ClientDetail from "./components/ClientDetail";

class ClientContainer extends React.Component {

    render() {
        const {client} = this.props;

        if (client.currentView === FORM_VIEW) {
            return (<ClientDetail />)
        }

        return <ClientList />
    }
}

function mapStateToProps(state) {
    return {
        client: state.clientReducer
    }
};

export default connect(mapStateToProps, null)(ClientContainer)