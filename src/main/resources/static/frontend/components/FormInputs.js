import React from 'react';

export const TextInput = ({input, label, type, className, placeholder, wrapper, meta: { touched, error, warning }}) => (
    <div className={"form-group "+ wrapper}>
        <label className="form-label">{label}</label>
        <input {...input} placeholder={placeholder} type={type} className={className} />
            {touched && ((error && <span className="text-danger">{error}</span>) || (warning && <span>{warning}</span>))}
    </div>
);

export const SelectInput = ({input, label, className, options, defaultValue, meta: { touched, error, warning }}) => (
    <div className="form-group mb-3">
        <label className="form-label">{label}</label>
        <div>
            <select {...input} placeholder={label} className={className}>
                <option value=''>{defaultValue}</option>
                {
                    options.map((option, key) => {
                        return <option key={key} value={option.id}>{ option.name }</option>
                    })
                }
            </select>
            {touched && ((error && <span className="text-danger">{error}</span>) || (warning && <span>{warning}</span>))}
        </div>
    </div>
);
