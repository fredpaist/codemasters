import * as React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {login} from "./LoginActions";

class LoginContainer extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            username: '',
            password: ''
        }

        this.submit = this.submit.bind(this);
    }

    submit(e) {
        e.preventDefault();
        this.props.login(this.state.username, this.state.password);
    };

    render() {
        const { user } = this.props;

        return (
            <div className="container">
                <div className="py-5 text-center">
                    <h2>Login</h2>
                </div>
                <div className="row">
                    <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                        {user.error &&
                        <div className="alert alert-danger" role="alert">
                            Check your data
                        </div>
                        }
                        <form className="needs-validation" onSubmit={this.submit}>
                            <div className="row">
                                <div className="col-12 mb-3">
                                    <label htmlFor="username">Username</label>
                                    <input type="text"
                                           className="form-control"
                                           id="username"
                                           onChange={(event) => this.setState({username: event.target.value})} />
                                </div>
                                <div className="col-12 mb-3">
                                    <label htmlFor="password">Password</label>
                                    <input type="password"
                                           className="form-control"
                                           id="password"
                                           onChange={(event) => this.setState({password: event.target.value})} />
                                </div>
                            </div>
                            <button className="btn btn-lg btn-primary">Login</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        user: state.loginReducer
    }
};

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        login
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer)
