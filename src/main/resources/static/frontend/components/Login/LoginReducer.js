import {LOGIN_CONTAINER_ERROR, LOGIN_CONTAINER_LOGIN, VALIDATE_USER_LOGIN} from "./LoginConstants";

const initialState = {
    error: '',
    loggedIn: false,
}

export default function loginReducer(state = initialState, action) {
    switch (action.type) {
        case LOGIN_CONTAINER_LOGIN:
            return Object.assign({}, state, {
                ...state,
                loggedIn: true,
                error: '',
            })
        case LOGIN_CONTAINER_ERROR:
            return Object.assign({}, state, {
                ...state,
                error: action.payload
            })
        case VALIDATE_USER_LOGIN:
            return Object.assign({}, state, {
                ...state,
                loggedIn: action.payload
            })
        default:
            return state;
    }
}