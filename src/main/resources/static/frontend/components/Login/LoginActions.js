import {
    LOGIN_CONTAINER_ERROR,
    LOGIN_CONTAINER_LOGIN,
    VALIDATE_USER_LOGIN,
} from "./LoginConstants";
import axios from "axios";

export const login = (username, password) => async dispatch => {
        await axios.post("login", {username: username, password: password})
        .then(response => {
            localStorage.setItem("accessToken", response.data.accessToken);
            dispatch({type: LOGIN_CONTAINER_LOGIN, payload: response.data});
        })
        .catch(error => {
            dispatch({type: LOGIN_CONTAINER_ERROR, payload: error});
        })
};

export const validateLogin = () => async dispatch => {
    const jwtHeader = {headers: {'Authorization': 'Bearer ' + localStorage.getItem("accessToken")}};

    await axios.get("api/validate/user", jwtHeader)
        .then(response => {
            dispatch({type: VALIDATE_USER_LOGIN, payload: response.data})
        })
        .catch(error => {
            localStorage.removeItem("accessToken");
            dispatch({type: VALIDATE_USER_LOGIN, payload: false})
        });
}