import React from "react"
import Login from "./components/Login/LoginContainer";
import {connect} from "react-redux";
import ClientContainer from "./components/Clients/ClientContainer";

class App extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { user } = this.props;

        if (!user.loggedIn) {
            return (<Login />)
        }

        return (
            <ClientContainer />
        )
    }
}

function mapStateToProps(state) {
    return {
        user: state.loginReducer
    }
}

export default connect(mapStateToProps, null)(App)