import {combineReducers} from 'redux';
import loginReducer from "./components/Login/LoginReducer";
import clientReducer from "./components/Clients/ClientReducer";
import { reducer as formReducer } from 'redux-form'

const rootReducer = combineReducers({
    loginReducer,
    clientReducer,
    form: formReducer
});

export default rootReducer;