package com.codemasters.service;

import com.codemasters.controller.dto.ClientListResponse;
import com.codemasters.controller.dto.ClientRequest;
import com.codemasters.controller.dto.ClientResponse;
import com.codemasters.domain.Client;
import com.codemasters.domain.Country;
import com.codemasters.domain.User;
import com.codemasters.repository.ClientRepository;
import com.codemasters.repository.CountryRepository;
import com.codemasters.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CountryRepository countryRepository;

    @Override
    public ClientListResponse getClients(Long userId) {
        List<Client> clients = clientRepository.findByUser(userRepository.getOne(userId));
        return new ClientListResponse(clients);
    }

    @Override
    public ResponseEntity<?> addClient(ClientRequest clientRequest, Long userId) {
        Client client = new Client();
        saveClient(client, clientRequest, userId);
        return new ResponseEntity("Client added successfully", HttpStatus.OK);
    }

    @Override
    public ResponseEntity<?> updateClient(Long clientId, ClientRequest clientRequest, Long userId) {
        Client client = clientRepository.findOneByIdAndUserId(clientId, userId);
        saveClient(client, clientRequest, userId);
        return new ResponseEntity("Client updated successfully", HttpStatus.OK);
    }

    @Override
    public ClientResponse getOneClient(Long clientId, Long userId) {
        Client client = clientRepository.findOneByIdAndUserId(clientId, userId);
        return new ClientResponse(client);
    }

    private void saveClient(Client client, ClientRequest request, Long userId) {
        client.setFirstName(request.getFirstName());
        client.setLastName(request.getLastName());
        client.setUsername(request.getUsername());
        client.setEmail(request.getEmail());
        client.setAddress(request.getAddress());

        Country country = countryRepository.getOne(request.getCountry());
        client.setCountry(country);
        User user = userRepository.getOne(userId);
        client.setUser(user);

        clientRepository.save(client);
    }

}
