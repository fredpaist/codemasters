package com.codemasters.service;

import com.codemasters.controller.dto.CountriesListResponse;

public interface CountryService {

    CountriesListResponse getCountries();

}
