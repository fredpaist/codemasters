package com.codemasters.service;

import com.codemasters.controller.dto.CountriesListResponse;
import com.codemasters.domain.Country;
import com.codemasters.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    @Autowired
    CountryRepository countryRepository;

    @Override
    public CountriesListResponse getCountries() {
        List<Country> countryList = countryRepository.findAll();
        return new CountriesListResponse(countryList);
    }
}
