package com.codemasters.service;

import com.codemasters.controller.dto.ClientListResponse;
import com.codemasters.controller.dto.ClientRequest;
import com.codemasters.controller.dto.ClientResponse;
import org.springframework.http.ResponseEntity;

public interface ClientService {

    ClientListResponse getClients(Long userId);

    ResponseEntity<?> addClient(ClientRequest clientRequest, Long userId);
    ResponseEntity<?> updateClient(Long clientId, ClientRequest clientRequest, Long userId);

    ClientResponse getOneClient(Long clientId, Long userId);
}
