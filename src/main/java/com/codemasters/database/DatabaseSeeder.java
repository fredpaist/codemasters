package com.codemasters.database;

import com.codemasters.domain.User;
import com.codemasters.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Add default users to database.
 *
 * @author Fred Paist
 */
@Component
public class DatabaseSeeder {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @EventListener
    public void seed(ContextRefreshedEvent event) {
        seedUsersTable();
    }

    private void seedUsersTable() {
        HashMap<String, String> userList = new HashMap<String, String>();
        userList.put("user_1", "user1password");
        userList.put("user_2", "user2password");
        userList.put("user_3", "user3password");

        for(Map.Entry<String, String> entry : userList.entrySet()) {
            User user = new User();
            user.setUsername(entry.getKey());
            user.setPassword(passwordEncoder.encode(entry.getValue()));
            userRepository.save(user);
        }
    }



}