package com.codemasters.repository;

import com.codemasters.domain.Client;
import com.codemasters.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClientRepository extends JpaRepository<Client, Long> {
    List<Client> findByUser(User user);

    Client findOneByIdAndUserId(Long clientId, Long userId);
}
