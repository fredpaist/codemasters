package com.codemasters.controller.dto;

import com.codemasters.domain.Client;

/**
 * Response for sending one client data.
 */
public class ClientResponse {

    private Long id;
    private String firstName;
    private String lastName;
    private String username;
    private String email;
    private String address;
    private Long country;

    public ClientResponse(Client client) {
        this.id = client.getId();
        this.firstName = client.getFirstName();
        this.lastName = client.getLastName();
        this.username = client.getUsername();
        this.email = client.getEmail();
        this.address = client.getAddress();
        this.country = client.getCountry().getId();
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public Long getCountry() {
        return country;
    }
}
