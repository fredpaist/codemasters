package com.codemasters.controller.dto;

import com.codemasters.domain.Client;

import java.util.ArrayList;
import java.util.List;

/**
 * Response for sending clients list to frontend.
 */
public class ClientListResponse {

    List<ClientResponse> clientResponseList = new ArrayList<>();

    public ClientListResponse(List<Client> clients) {
        for (Client client: clients) {
            ClientResponse clientResponse = new ClientResponse(client);
            clientResponseList.add(clientResponse);
        }
    }

    public List<ClientResponse> getClientResponseList() {
        return clientResponseList;
    }

    private class ClientResponse {
        private Long id;
        private String firstName;
        private String lastName;
        private String username;

        public ClientResponse(Client client) {
            this.id = client.getId();
            this.firstName = client.getFirstName();
            this.lastName = client.getLastName();
            this.username = client.getUsername();
        }

        public Long getId() {
            return id;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getUsername() {
            return username;
        }
    }
}
