package com.codemasters.controller.dto;

/**
 * Response for sending JWT token to front-end.
 */
public class UserAuthenticationResponse {
    private String accessToken;

    public UserAuthenticationResponse(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}

