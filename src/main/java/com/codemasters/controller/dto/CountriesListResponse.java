package com.codemasters.controller.dto;

import com.codemasters.domain.Country;

import java.util.ArrayList;
import java.util.List;

/**
 * Response for countries list.
 */
public class CountriesListResponse {

    List<CountryResponse> countryResponseList = new ArrayList<>();

    public CountriesListResponse(List<Country> countries) {
        for (Country country : countries) {
            CountryResponse countryResponse = new CountryResponse(country);
            countryResponseList.add(countryResponse);
        }
    }

    public List<CountryResponse> getCountryResponseList() {
        return countryResponseList;
    }

    private class CountryResponse {
        private String name;
        private Long id;

        public CountryResponse(Country country) {
            this.id = country.getId();
            this.name = country.getName();

        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }
}
