package com.codemasters.controller;

import com.codemasters.controller.dto.CountriesListResponse;
import com.codemasters.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/countries")
public class CountryController {

    @Autowired
    CountryService countryService;

    /**
     * Get countries list.
     *
     * @return CountriesListResponse
     */
    @GetMapping
    public CountriesListResponse getCountries() {
        return countryService.getCountries();
    }
}
