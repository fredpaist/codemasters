package com.codemasters.controller;

import com.codemasters.controller.dto.ClientListResponse;
import com.codemasters.controller.dto.ClientRequest;
import com.codemasters.controller.dto.ClientResponse;
import com.codemasters.security.CurrentUser;
import com.codemasters.security.UserPrincipal;
import com.codemasters.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Controller for client crud operations.
 *
 * @author Fred Paist
 */
@RestController
@RequestMapping("api/clients")
public class ClientController {

    @Autowired
    ClientService clientService;

    /**
     * Get current logged in user clients.
     *
     * @param currentUser - logged in account principal.
     * @return ClientListResponse
     */
    @GetMapping
    public ClientListResponse getClients(@CurrentUser UserPrincipal currentUser) {
        return clientService.getClients(currentUser.getId());
    }

    /**
     * Add new client to logged in user.
     *
     * @param request     - Client data
     * @param currentUser - logged in account principal.
     * @return ResponseEntity
     */
    @PostMapping
    public ResponseEntity<?> addClient(@Valid @RequestBody ClientRequest request, @CurrentUser UserPrincipal currentUser) {
        return clientService.addClient(request, currentUser.getId());
    }

    /**
     * Add new client to logged in user.
     *
     * @param request     - Client data
     * @param currentUser - logged in account principal.
     * @return ResponseEntity
     */
    @PutMapping("{id}")
    public ResponseEntity<?> updateClient(@PathVariable Long id, @Valid @RequestBody ClientRequest request, @CurrentUser UserPrincipal currentUser) {
        return clientService.updateClient(id, request, currentUser.getId());
    }

    /**
     * Get one client for logged in user.
     *
     * @param id          - Client id.
     * @param currentUser - logged in account principal.
     * @return ClientResponse
     */
    @GetMapping("{id}")
    public ClientResponse getOneClient(@PathVariable Long id, @CurrentUser UserPrincipal currentUser) {
        return clientService.getOneClient(id, currentUser.getId());
    }
}
