package com.codemasters.controller;

import com.codemasters.controller.dto.LoginRequest;
import com.codemasters.controller.dto.UserAuthenticationResponse;
import com.codemasters.security.CurrentUser;
import com.codemasters.security.JwtTokenProvider;
import com.codemasters.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Controller for account authentication
 *
 * @author Fred Paist
 */
@RestController
public class LoginController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider tokenProvider;

    /**
     * Endpoint is used to initiate the account process in Admin and Client applications.
     *
     * @param loginRequest - A plain username and password string request with validation.
     * @return ResponseEntity with jwt token attached.
     */
    @PostMapping("login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new UserAuthenticationResponse(jwt));
    }

    /**
     * End point for validation if account is logged in.
     *
     * @param currentUser - Current account base on the authorization header.
     * @return boolean - if account base exists.
     * @author Fred Paist
     */
    @GetMapping("api/validate/user")
    public Boolean validateUser(@CurrentUser UserPrincipal currentUser) {
        return currentUser != null;
    }
}
